"""neab URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from rest_framework.routers import DefaultRouter
from apps.content.views import LectureViewSet
from apps.content.views import NewsViewSet
from apps.content.views import AboutViewSet, InitialPageViewSet
from apps.communication.views import ContactUsViewSet
from fcm_django.api.rest_framework import FCMDeviceViewSet


router = DefaultRouter()

router.register('v1/content/lecture', LectureViewSet)
router.register('v1/content/news', NewsViewSet)
router.register('v1/communication/contact_us', ContactUsViewSet)
router.register('v1/content/about', AboutViewSet)
router.register('v1/content/initial_page', InitialPageViewSet)
router.register(r'v1/devices', FCMDeviceViewSet)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('^ckeditor/', include('ckeditor_uploader.urls')),
    path('', include(router.urls)),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
