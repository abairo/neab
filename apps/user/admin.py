from django.contrib import admin
from .models import User, Address
from fcm_django.models import FCMDevice


class UserAdmin(admin.ModelAdmin):
    """Book view"""
    list_display = ('username', 'first_name', 'last_name')
    search_fields = ('first_name', 'last_name', 'username')
    readonly_fields = ('last_login', 'date_joined')
    fieldsets = (
        ('Perfil', {
           'fields': ('first_name',
                      'last_name',
                      'bio',
                      'birth_date',
                      'image',
                      'email',
                      'phone',
                      'cpf')
        }),
        ('Usuário', {
            'fields': ('username',
                       'password', 
                       'is_active', 
                       'is_staff', 
                       'is_superuser',
                       'date_joined', 
                       'last_login'),
        }),
        ('Permissões', {
            'fields': (
                'groups', 
                'user_permissions'                        
            )
        })
    )

    def save_model(self, request, obj, form, change):
        if change:
            # User Update
            if form.changed_data and 'password' in form.changed_data:
                # User update and password changed
                obj.set_password(obj.password)
                obj.save()
            else:
                # User update without password has changed
                obj.save()
        else:
            # User creation
            obj.set_password(obj.password)
            obj.save()


class AddressAdmin(admin.ModelAdmin):
    """Book view"""
    list_display = ('user', 'street', 'city', 'neighborhood')
    search_fields = ('user', 'street', 'city', 'neighborhood')
    list_filter = ('city', 'neighborhood')


admin.site.register(User, UserAdmin)
admin.site.register(Address, AddressAdmin)
admin.site.unregister(FCMDevice)
