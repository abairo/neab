from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.translation import ugettext_lazy as _


class User(AbstractUser):
    first_name = models.CharField(_('Nome'), max_length=50, blank=True)
    last_name = models.CharField(_('Sobrenome'), max_length=50, blank=True)
    bio = models.TextField(_('biografia'), max_length=500, blank=True)
    birth_date = models.DateField(_('Data de nascimento'), null=True, blank=True)
    image = models.ImageField(_('imagem'), upload_to='users/%Y/%m/%d', null=True, blank=True)
    phone = models.CharField(_('telefone'), max_length=15, null=True, blank=True)
    cpf = models.CharField(_('cpf'), max_length=15, null=True, blank=True)

    class Meta:
        verbose_name = 'Usuário'
        verbose_name_plural = 'Usuários'

    def __str__(self):
        return '{}'.format(self.username)


class Address(models.Model):
    street = models.CharField(_('Rua'), max_length=50)
    number = models.CharField(_('Número'), max_length=10)
    neighborhood = models.CharField(_('Bairro'), max_length=50)
    city = models.CharField(_('Cidade'), max_length=30, null=True, blank=True)
    state = models.CharField(_('Estado'), max_length=30, default='Paraná', null=True, blank=True)
    user = models.OneToOneField(User, on_delete=models.CASCADE, verbose_name=_('Usuário'))

    class Meta:
        verbose_name = 'Endereço'
        verbose_name_plural = 'Endereços'

    def __str__(self):
        return '{} - {}, {}'.format(self.neighborhood, self.street, self.number)
