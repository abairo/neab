from django.apps import AppConfig


class ActivityConfig(AppConfig):
    name = 'apps.activity'
    verbose_name = 'Atividade'
