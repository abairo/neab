from django.db import models
from django.utils.translation import ugettext_lazy as _
from apps.user.models import User


class Activity(models.Model):
    name = models.CharField(_('nome'), max_length=150)
    description = models.TextField(_('descrição'))
    user = models.ForeignKey(User, on_delete=models.PROTECT, verbose_name=_('usuário'))

    def __str__(self):
        return '{} - {}'.format(self.name, self.user.first_name)

    class Meta:
        verbose_name = 'Atividade'
        verbose_name_plural = 'Atividades'

    @property
    def total(self):
        "Returns the total hours"        
        if self.user_hours.first():            
            duration = self.user_hours.aggregate(res=models.Sum(models.F('duration'))).get('res', 0)
            return '{} horas informadas'.format(str(duration))
        else:
            return '0h'


class UserActivity(models.Model):
    activity = models.ForeignKey(Activity, on_delete=models.PROTECT, verbose_name=_('atividade'), related_name='user_hours')
    user = models.ForeignKey(User, on_delete=models.PROTECT, verbose_name=_('usuário'), null=True, blank=True)
    duration = models.DurationField(_('duração'))
    date = models.DateField(_('data'))
    created_at = models.DateTimeField(_('criado em'), auto_now_add=True)
    updated_at = models.DateTimeField(_('atualizado em'), auto_now=True)

    def __str__(self):
        return '{} - {} - {}'.format(self.activity.name, self.user.first_name, str(self.duration))

    class Meta:
        verbose_name = 'Atividade do usuário'
        verbose_name_plural = 'Atividades do usuário'
