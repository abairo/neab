from django.contrib import admin
from .models import Activity, UserActivity


class ActivityAdmin(admin.ModelAdmin):
    """Activity view"""
    list_display = ('name', 'description')
    readonly_fields = ('total',)
    search_fields = ('id', 'name', 'description')
    list_filter = ('name',)


class UserActivityAdmin(admin.ModelAdmin):
    """Activity view"""
    list_display = ('activity', 'user')
    search_fields = ('id', 'activity__name', 'user__first_name')
    list_filter = ('activity',)
    readonly_fields = ('user',)

    def save_model(self, request, obj, form, change):
        obj.user = request.user
        super().save_model(request, obj, form, change)


admin.site.register(Activity, ActivityAdmin)
admin.site.register(UserActivity, UserActivityAdmin)