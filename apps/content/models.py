from django.db import models
from ckeditor_uploader.fields import RichTextUploadingField
from django.utils.translation import ugettext_lazy as _
from django.utils.html import format_html
from django.dispatch import receiver
from django.db.models.signals import post_save
from fcm_django.models import FCMDevice


"""
    ABOUT
"""


class About(models.Model):
    title = models.CharField(_('título'), null=False, blank=False, max_length=255)
    content = RichTextUploadingField(_('conteúdo'), null=False, blank=False)
    published = models.BooleanField(_('publicado'), default=False)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Sobre'
        verbose_name_plural = 'Sobre'


"""
    INITIAL PAGE
"""


class InitialPage(models.Model):
    title = models.CharField(_('título'), null=False, blank=False, max_length=255)
    content = RichTextUploadingField(_('conteúdo'), null=False, blank=False)
    published = models.BooleanField(_('publicado'), default=False)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Página inicial'
        verbose_name_plural = 'Página inicial'    


"""
    SPEAKER
"""


class Speaker(models.Model):
    name = models.CharField(_('nome'), max_length=150)
    bio = models.TextField(_('biografia'), max_length=500)
    phone = models.CharField(_('telefone'), max_length=15)
    image = models.ImageField(_('imagem'), upload_to='speaker/%Y/%m/%d', null=True, blank=True)
    email = models.EmailField(_('email'), blank=True)

    class Meta:
        verbose_name = 'Palestrante'
        verbose_name_plural = 'Palestrantes'

    def __str__(self):
        return '{} - {}'.format(self.name, self.email)


"""
    LECTURE
"""


class Lecture(models.Model):
    title = models.CharField(_('título'), max_length=150)
    description = models.TextField(_('descrição'))
    image = models.ImageField(_('imagem'), upload_to='lecture/%Y/%m/%d')
    content = RichTextUploadingField(null=True, blank=True)
    speaker = models.ForeignKey(Speaker, verbose_name='palestrante', on_delete=models.PROTECT, null=True, blank=True)

    class Meta:
        verbose_name = 'Palestra'
        verbose_name_plural = 'Palestras'

    def image_tag(self):
        return format_html('<img href="{0}" src="{0}" width="100" height="100" />'.format(self.image.url))

    image_tag.allow_tags = True
    image_tag.short_description = 'Image'

    def __str__(self):
        if self.speaker:
            return '{} - {}'.format(self.title, self.speaker)
        else:
            return '{} - {}'.format(self.title, 'a definir')


"""
    NEWS
"""


class News(models.Model):
    title = models.CharField(_('título'), max_length=150)
    description = models.TextField(_('descrição'))
    image = models.ImageField(_('imagem'), upload_to='news/%Y/%m/%d')
    content = RichTextUploadingField(null=True, blank=True)

    class Meta:
        verbose_name = 'Notícia'
        verbose_name_plural = 'Notícias'

    def image_tag(self):
        return format_html('<img href="{0}" src="{0}" width="100" height="100" />'.format(self.image.url))

    image_tag.allow_tags = True
    image_tag.short_description = 'Image'

    def __str__(self):
        return '{} - {}'.format(self.id, self.title)


"""
    SIGNALS
"""


def truncate(string, width):
    if len(string) > width:
        string = string[:width-3] + '...'
    return string


@receiver(post_save, sender=Lecture)
def lecture_handler(sender, **kwargs):
    if kwargs['created']:
        devices = FCMDevice.objects.filter(active=True)
        
        lecture_title = kwargs['instance'].title
        lecture_description = kwargs['instance'].description
        
        devices.send_message(title=truncate(lecture_title, 30), 
                             body=truncate(lecture_description, 35))


@receiver(post_save, sender=News)
def news_handler(sender, **kwargs):
    if kwargs['created']:
        devices = FCMDevice.objects.filter(active=True)
        
        news_title = kwargs['instance'].title
        news_description = kwargs['instance'].description
        
        devices.send_message(title=truncate(news_title, 30), 
                             body=truncate(news_description, 35))
