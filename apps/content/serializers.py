from rest_framework import serializers
from .models import About, InitialPage, Lecture, News
from django.conf import settings


class AboutSerializer(serializers.ModelSerializer):
    class Meta:
        model = About
        exclude = ('published', 'title', 'id')


class InitialPageSerializer(serializers.ModelSerializer):
    class Meta:
        model = InitialPage
        exclude = ('published', 'title', 'id')


class LectureSerializer(serializers.ModelSerializer):

    class Meta:
        model = Lecture
        fields = ('__all__')

    def to_representation(self, instance):
        lecture = super(LectureSerializer, self).to_representation(instance)
        content = lecture['content']
        lecture['content'] = content.replace('/media/uploads', settings.CKEDITOR_MEDIA_SERVER + 'media/uploads')
        
        return lecture


class NewsSerializer(serializers.ModelSerializer):

    class Meta:
        model = News
        fields = ('__all__')

    def to_representation(self, instance):
        news = super(NewsSerializer, self).to_representation(instance)
        content = news['content']
        news['content'] = content.replace('/media/uploads', settings.CKEDITOR_MEDIA_SERVER + 'media/uploads')
        
        return news
