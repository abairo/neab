from django.shortcuts import render
from rest_framework import viewsets, permissions
from .models import About, InitialPage, Lecture, News
from .serializers import (AboutSerializer, 
                          InitialPageSerializer, 
                          LectureSerializer, 
                          NewsSerializer)


class AboutViewSet(viewsets.ReadOnlyModelViewSet):
    permission_classes = (permissions.AllowAny,)
    queryset = About.objects.all()
    serializer_class = AboutSerializer

    def get_queryset(self, *args, **kwargs):
        return About.objects.filter(published=True)


class InitialPageViewSet(viewsets.ReadOnlyModelViewSet):
    permission_classes = (permissions.AllowAny,)
    queryset = InitialPage.objects.all()
    serializer_class = InitialPageSerializer

    def get_queryset(self, *args, **kwargs):
        return InitialPage.objects.filter(published=True)


class NewsViewSet(viewsets.ReadOnlyModelViewSet):
    permission_classes = (permissions.AllowAny,)
    serializer_class = NewsSerializer
    queryset = News.objects.all()


class LectureViewSet(viewsets.ReadOnlyModelViewSet):
    permission_classes = (permissions.AllowAny,)
    serializer_class = LectureSerializer
    queryset = Lecture.objects.all()
