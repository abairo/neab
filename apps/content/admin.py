from django.contrib import admin
from .models import About, InitialPage, News, Lecture, Speaker


class InitialPageAdmin(admin.ModelAdmin):
    """Lecture view"""
    list_display = ('title', 'published')
    search_fields = ('title', 'published')
    list_filter = ('published',)

    def save_model(self, request, obj, form, change):
        if obj.published:
            obj.__class__.objects.all().update(published=False)
        
        obj.save()


class AboutPageAdmin(admin.ModelAdmin):
    """Lecture view"""
    list_display = ('title', 'published')
    search_fields = ('title', 'published')
    list_filter = ('published',)

    def save_model(self, request, obj, form, change):
        if obj.published:
            obj.__class__.objects.all().update(published=False)

        obj.save()


class LectureAdmin(admin.ModelAdmin):
    """Lecture view"""
    list_display = ('title', 'description', 'image_tag')
    search_fields = ('id', 'title', 'description')
    list_filter = ('title',)


class SpeakerAdmin(admin.ModelAdmin):
    """Lecture view"""
    list_display = ('name', 'email')
    search_fields = ('name', 'email')


admin.site.register(About, AboutPageAdmin)
admin.site.register(InitialPage, InitialPageAdmin)
admin.site.register(News)
admin.site.register(Lecture, LectureAdmin)
admin.site.register(Speaker, SpeakerAdmin)
