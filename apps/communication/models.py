from django.db import models
from django.utils.translation import ugettext_lazy as _


class ContactUs(models.Model):
    name = models.CharField(_('nome'), max_length=255)
    email = models.CharField(_('email'), max_length=255)
    phone = models.CharField(_('telefone'), max_length=15)
    description = models.TextField(_('descrição'))
    created_at = models.DateTimeField(_('criado em'), auto_now_add=True)
    verified = models.BooleanField(_('verificado'), default=False)

    class Meta:
        verbose_name = 'Fale conosco'
        verbose_name_plural = 'Fale conosco'

    def __str__(self):
        return f'{self.name} - {self.created_at}'
