from django.contrib import admin
from .models import ContactUs


class ContactUsAdmin(admin.ModelAdmin):
    """Contact Us view"""
    list_display = ('name', 'created_at', 'verified')
    search_fields = ('name', 'created_at', 'verified')
    list_filter = ('name', 'created_at', 'verified')
    list_editable = ('verified',)

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def get_ordering(self, request):
        return ['verified', '-created_at']


admin.site.register(ContactUs, ContactUsAdmin)