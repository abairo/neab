from django.apps import AppConfig


class CommunicationConfig(AppConfig):
    name = 'apps.communication'
    verbose_name = 'Comunicação'
