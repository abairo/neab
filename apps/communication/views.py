from rest_framework import mixins, viewsets
from .models import ContactUs
from .serializers import ContactUsSerializer
from rest_framework import permissions


class ContactUsViewSet(mixins.CreateModelMixin, viewsets.GenericViewSet):
    permission_classes = (permissions.AllowAny,)
    serializer_class = ContactUsSerializer
    queryset = ContactUs.objects.all()
