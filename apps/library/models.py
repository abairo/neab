from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.utils.html import format_html
from ckeditor_uploader.fields import RichTextUploadingField
from apps.user.models import User


class Book(models.Model):
    title = models.CharField(_('título'), max_length=150)
    description = models.TextField(_('descrição'))
    image = models.ImageField(_('imagem'), upload_to='library/%Y/%m/%d')
    author = models.CharField(_('autor'), default='Desconhecido', max_length=250)
    
    def image_tag(self):
        return format_html('<img href="{0}" src="{0}" width="150" height="150" />'.format(self.image.url))

    image_tag.allow_tags = True
    image_tag.short_description = 'Image'

    class Meta:
        verbose_name = 'Livro'
        verbose_name_plural = 'Livros'

    def __str__(self):
        return '{} - {}'.format(self.id, self.title)

    
class BookLoan(models.Model):
    LOANED = 1
    RETURNED = 2
    BOOKLOAN_STATUS = (
        (LOANED, 'Emprestado'),
        (RETURNED, 'Devolvido')        
    )

    user = models.ForeignKey(User, verbose_name=_('Usuário'), on_delete=models.PROTECT)
    book = models.ForeignKey(Book, verbose_name=_('Livro'), on_delete=models.PROTECT)
    loaned_at = models.DateField(_('Emprestado em'))
    returned_at = models.DateField(_('Devolvido em'), null=True, blank=True)
    status = models.IntegerField(choices=BOOKLOAN_STATUS, default=LOANED)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = 'Livro emprestado'
        verbose_name_plural = 'Livros emprestados'

    def __str__(self):
        return '{} - {} - {}'.format(self.user.username, self.book.title, self.BOOKLOAN_STATUS[self.status - 1][1])
