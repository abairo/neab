from django.contrib import admin
from .models import Book, BookLoan


class BookAdmin(admin.ModelAdmin):
    """Book view"""
    list_display = ('title', 'description', 'image_tag')
    search_fields = ('id', 'title', 'description')
    list_filter = ('title',)


class BookLoanAdmin(admin.ModelAdmin):
    """Book view"""
    list_display = ('user', 'book', 'status')
    search_fields = ('user', 'book', 'status')
    list_filter = ('status', 'book',)


admin.site.register(Book, BookAdmin)
admin.site.register(BookLoan, BookLoanAdmin)