from django.contrib import admin
from django.core.files.storage import FileSystemStorage
from django.http import HttpResponse
from django.template.loader import render_to_string
from .models import Report
from apps.activity.models import UserActivity
from weasyprint import HTML
from django_object_actions import DjangoObjectActions
from django.db.models import F


@admin.register(Report)
class ReportAdmin(DjangoObjectActions, admin.ModelAdmin):
    change_form_template = "reports/custom_report.html"
    # search_fields = ['title']
    # list_display = ('title', 'date')

    # def get_form(self, request, obj=None, **kwargs):
    #     # self.exclude = []
    #     if not request.user.is_superuser:
    #         self.exclude.append('Permissions')
    #     return super(ReportAdmin, self).get_form(request, obj, **kwargs)
    def generate_pdf(self, request, obj):
        filters = dict()
        if obj.date_begin and obj.date_end:
            filters['date__range'] = (obj.date_begin, obj.date_end)
        if obj.user:
            filters['user__id'] = obj.user.id
        
        activities = UserActivity.objects.filter(
            **filters
        ).filter(
            user=obj.user
        ).annotate(
            user_name=F('user__first_name'), 
            horas=F('duration'),
            date_activity=F('date')
        )
        
        html_string = render_to_string('reports/pdf_template.html', {'activities': activities})

        html = HTML(string=html_string)
        
        html.write_pdf(target='/tmp/{}.pdf'.format(obj));

        fs = FileSystemStorage('/tmp')
        with fs.open('{}.pdf'.format(obj)) as pdf:
            response = HttpResponse(pdf, content_type='application/pdf')
            response['Content-Disposition'] = 'attachment; filename="{}.pdf"'.format(obj)
            return response

        return response
    #generate_pdf.label = 'Gerar PDF'
    #generate_pdf.short_description = 'Clique para gerar o PDF dessa ordem de serviço'

    #change_actions = ('generate_pdf',)

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def response_change(self, request, obj):
        return self.generate_pdf(request, obj)