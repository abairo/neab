from django.db import models
from apps.user.models import User
from django.utils.translation import ugettext_lazy as _


class Report(models.Model):

    title = models.CharField(_('Título'), max_length=100, blank=True, null=True)
    date_begin = models.DateField(_('Data inicial'), blank=True, null=True)
    date_end = models.DateField(_('Data final'), blank=True, null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, 
                             null=True, verbose_name=_('Usuário'))
    description = models.TextField(_('Descrição'))

    def __str__(self):
        return self.title
    
    class Meta:
        verbose_name = 'Relatório'
        verbose_name_plural = 'Relatórios'
        ordering = ['-id']
